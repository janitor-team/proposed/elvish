#!/bin/sh

set -ex

cd website || exit 1
for file in ref/*.md; do
	name=$(basename "$file" .md)
	test "$name" = "prelude" && continue
	"$BIN_PATH/macros" -elvdoc "$BIN_PATH/elvdoc" -repo ../ < "$file" |
		pandoc -s -f gfm+smart -t man -o ../debian/elvish-"$name".7 \
			-V section:7 \
			-V "header:Miscellaneous Information Manual" \
			-V "footer:Elvish $DEB_VERSION_UPSTREAM" \
			-M "title:elvish-$name" \
			-M "date:$(date -u --date="@$SOURCE_DATE_EPOCH" "+%b %d, %Y")"
done
